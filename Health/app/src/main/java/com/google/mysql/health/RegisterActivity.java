package com.google.mysql.health;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    Button btn_reg;
    EditText username, email, password, age, phone;
    String gender;
    AlertDialog.Builder builder;
    String reg_url="http://192.168.1.74/officeapplication/register.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_register );
        btn_reg = findViewById( R.id.btnRegister );
        username = findViewById( R.id.etUsername );
        email = findViewById( R.id.etEmail );
        password = findViewById( R.id.etPassword );
        age = findViewById( R.id.etAge );
        phone = findViewById( R.id.etPhone );



        btn_reg.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String str_username = username.getText().toString();
                final String str_email = email.getText().toString();
                final String str_password = password.getText().toString();
                final String str_age = age.getText().toString();
                final String str_phone = phone.getText().toString();
                final String str_gender = gender;
                builder= new AlertDialog.Builder( RegisterActivity.this );




                   if (str_username.equals( "" )||str_email.equals( "" )||str_password.equals( "" )||str_age.equals( "" )||str_phone.equals( "" )) {
                       builder.setTitle( "Something went wrong........." );
                       builder.setMessage( "Please fill all the fields" );
                       displayAlert("input_error");

                    }
                    else {

                            StringRequest stringRequest = new StringRequest( Request.Method.POST, reg_url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONArray jsonarray = new JSONArray(response);
                                    JSONObject jsonObject= jsonarray.getJSONObject( 0 );
                                    String code=jsonObject.getString( "code" );
                                    String message = jsonObject.getString( "message" );
                                    builder.setTitle( "Server Response..." );
                                    builder.setMessage(message);
                                    displayAlert(code);

                                           /* Intent intent = new Intent (RegisterActivity.this, login.class);
                                            startActivity(intent);
                                            finish();*/

                                    } catch (JSONException e) {
                                        e.printStackTrace();

                                }


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(RegisterActivity.this,"Error",Toast.LENGTH_LONG).show();
                                error.printStackTrace();

                            }
                        } ) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                params.put( "Name", str_username.trim());
                                params.put( "Email", str_email.trim());
                                params.put( "Password", str_password.trim());
                                params.put( "Age", str_age.trim());
                                params.put( "Phone", str_phone.trim());
                                params.put( "Gender", gender);


                                return params;
                            }
                        };

                        MySingleton.getInstance( RegisterActivity.this ).addToRequestque(stringRequest);


                    }
                }




        });

}

    public void onRadioButtonClicked(View view) {
        //Is the button now Checked?
        boolean checked = ((RadioButton) view).isChecked();

        //Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radioMale:
                if (checked)
                    gender = "Male";
                break;
            case R.id.radioFemale:
                if (checked)
                    gender = "Female";
                break;
        }
    }


    public void displayAlert(final String code)
    {
        builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               if(code.equals( "input_error" )) {
                   password.setText("");
               }

               else if (code.equals ("reg_success"))
               {
                   finish();
               }
               else if (code.equals ("reg_failed"))
               {
                   username.setText("");
                   email.setText("");
                   password.setText("");
                   age.setText("");
                   phone.setText("");
               }
            }

        } );

        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }

}



package com.google.mysql.health;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {
EditText login_email,login_password;
String lg_email,lg_password;
Button Btn_login;
Button Btn_register;
AlertDialog.Builder builder;
SessionManagement session;
Context context;

String login_url="http://192.168.1.74/officeapplication/login.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );



        //Session Manager
       session = new SessionManagement(getApplicationContext());

        Btn_login=findViewById( R.id.btLogin );
        Btn_register=findViewById( R.id.btSignUp );

        Btn_register.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent (login.this, RegisterActivity.class);
                startActivity(intent);



            }

        });

login_email=findViewById( R.id.eUsername );
login_password=findViewById( R.id.ePassword );

Btn_login.setOnClickListener( new View.OnClickListener() {

    @Override
    public void onClick(View v) {


        lg_email=login_email.getText().toString();
        lg_password=login_password.getText().toString();
        builder= new AlertDialog.Builder( login.this );
        if (lg_email.equals("")||lg_password.equals("")){

            builder.setTitle( "Something went wrong" );
            displayAlert( "Enter a valid username and password" );



        }

        else
        {

            StringRequest stringRequest = new StringRequest( Request.Method.POST, login_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONArray jsonarray = new JSONArray(response);
                        JSONObject jsonObject= jsonarray.getJSONObject( 0 );
                        String code=jsonObject.getString( "code" );
                       if (code.equals( "login_failed" ))
                       {
                           builder.setTitle( "LOGIN ERROR" );
                           displayAlert( jsonObject.getString( "message" ));
                       }
                        else
                       {
                           Toast.makeText(login.this,"LOGIN SUCCESS",Toast.LENGTH_LONG).show();
                           String email = login_email.getText().toString();
                           String password = login_password.getText().toString();
                           session.createLoginSession(email,password);
                           startActivity(new Intent(login.this, HomeActivity.class));

                           /*SharedPreManager.getInstance(getApplicationContext())
                                   .userLogin(
                                           jsonObject.getString("username"),
                                           jsonObject.getString("email")
                                   );


                           finish();*/

                       }



                    } catch (JSONException e) {
                        e.printStackTrace();

                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                   /* NetworkResponse response = error.networkResponse;
                    if (error instanceof ServerError && response != null) {
                        try {
                            String res = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, "utf-8"));
                            // Now you can use any deserializer to make sense of data
                            JSONObject obj = new JSONObject(res);
                        } catch (UnsupportedEncodingException e1) {
                            // Couldn't properly decode data to string
                            e1.printStackTrace();
                        } catch (JSONException e2) {
                            // returned data is not JSONObject?
                            e2.printStackTrace();
                        }
                    }*/
                    Toast.makeText(login.this,"Error",Toast.LENGTH_LONG).show();
                    error.printStackTrace();

                }
            } ) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();

                    params.put( "Email", lg_email.trim());
                    params.put( "Password", lg_password.trim());



                    return params;
                }
            };

            MySingleton.getInstance( login.this ).addToRequestque(stringRequest);


        }
        }

} );


    }

    public void displayAlert(final String message)
    {
        builder.setMessage( message );
        builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                login_email.setText( "" );
                login_password.setText( "" );

                }


        } );

        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }



}

package com.google.mysql.health;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;
import java.util.Random;

public class HomeActivity extends AppCompatActivity {

    Dialog myDialog;
    String address="https://api.myjson.com/bins/9e5dq";
    InputStream is=null;
    String line=null;
    String result=null;
    String[] data;
    int counter=0;
    long date = System.currentTimeMillis();
    ImageView btnmap ,btnsettings;
    Button btnnews,btndisease;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        myDialog = new Dialog(this);

        //Allow network in main thread
        StrictMode.setThreadPolicy((new StrictMode.ThreadPolicy.Builder().permitNetwork().build()));

        getData();

        btnnews=findViewById(R.id.news);
        btnnews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });

        btndisease=findViewById( R.id.disease );
        btndisease.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (HomeActivity.this, DiseaseActivity.class);
                startActivity(intent);
            }
        } );

        btnmap=findViewById(R.id.imap);
        btnmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,MapsActivity2.class);
                startActivity(intent);
            }
        });

        btnsettings=findViewById(R.id.settings);
        btnmap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,SettingsActivity.class);
                startActivity(intent);
            }
        });



    }



    public void ShowPopup(View view) {
        ImageButton imclose;
        Button btnprev, btnnext;
        final TextView txttips;
        myDialog.setContentView(R.layout.activity_health_tips);
        imclose =myDialog.findViewById(R.id.ib_close);
        btnprev = myDialog.findViewById(R.id.btnprevious);
        btnnext = myDialog.findViewById(R.id.btnnext);
        txttips = myDialog.findViewById(R.id.tv_tips);

        Random r = new Random();

        counter = r.nextInt(149);

            txttips.setText(data[counter]);

        btnnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                counter++;
                if(counter==data.length)
                    counter=0;
                txttips.setText(data[counter]);
            }
        });

        btnprev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter--;
                //If counter has come to starting of array.set it to last item.
                if(counter<0)
                    counter=data.length-1;
                txttips.setText(data[counter]);
            }
        });

        imclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myDialog.dismiss();
            }
        });
        Objects.requireNonNull(myDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();
    }

    private void getData()
    {
        try
        {
            URL url = new URL(address);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("GET");

            is = new BufferedInputStream(con.getInputStream());

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        //READ IS CONTENT INTO A STRING
        try {
            BufferedReader br=new BufferedReader(new InputStreamReader(is));
            StringBuilder sb=new StringBuilder();

            while((line=br.readLine()) != null)
            {
                sb.append(line).append("\n");
            }
            is.close();
            result=sb.toString();

        }catch (Exception e)
        {
           e.printStackTrace();
        }

        //PARSE JSON DATA
        try
        {
            JSONArray jsonArray=new JSONArray(result);
            JSONObject jsonObject;

            data = new String[jsonArray.length()];

            for(int i=0;i<jsonArray.length();i++)
            {
                jsonObject=jsonArray.getJSONObject(i);
                data[i]=jsonObject.getString("health");

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

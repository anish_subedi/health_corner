package com.google.mysql.health;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreManager {
    private static SharedPreManager mInstance;
    private  static Context mCtx;
    private static final String SHARED_PREF_NAME="mysharedpref12";
    private static final String KEY_USERNAME="username";
    private static final String KEY_USER_EMAIL="useremail";


     private SharedPreManager(Context context){
         mCtx=context;
     }

     public static synchronized SharedPreManager getInstance(Context context){
         if(mInstance == null){
             mInstance=new SharedPreManager( context );
         }
         return mInstance;
     }
      public boolean userLogin( String username, String email){
          SharedPreferences sharedPreferences=mCtx.getSharedPreferences( SHARED_PREF_NAME, Context.MODE_PRIVATE );
          SharedPreferences.Editor editor= sharedPreferences.edit();
          editor.putString( KEY_USER_EMAIL,email);
          editor.putString( KEY_USERNAME,username );

          editor.apply();
          return true;
      }

      public boolean isloggedIn()
      {
          SharedPreferences sharedPreferences=mCtx.getSharedPreferences( SHARED_PREF_NAME, Context.MODE_PRIVATE );
          if(sharedPreferences.getString( KEY_USER_EMAIL,null )!=null){
              return true;
          }
          return false;
      }

      public boolean logout(){
          SharedPreferences sharedPreferences=mCtx.getSharedPreferences( SHARED_PREF_NAME, Context.MODE_PRIVATE );
          SharedPreferences.Editor editor= sharedPreferences.edit();
          editor.clear();
          editor.apply();
          return true;

      }

      public String getUsername(){

          SharedPreferences sharedPreferences=mCtx.getSharedPreferences( SHARED_PREF_NAME, Context.MODE_PRIVATE );
          return sharedPreferences.getString( KEY_USERNAME,null );

      }

      public String getUserEmail(){
          SharedPreferences sharedPreferences=mCtx.getSharedPreferences( SHARED_PREF_NAME, Context.MODE_PRIVATE );
          return sharedPreferences.getString( KEY_USER_EMAIL,null );

      }
}
